var gulp = require('gulp'),
    pug = require('gulp-pug'),
    browserSync = require('browser-sync').create(),
    postcss = require('gulp-postcss'),
    sass = require('gulp-sass'),
    autoprefixer = require('autoprefixer'),
    rucksack = require('rucksack-css'),
    concat = require('gulp-concat'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    pump = require('pump'),
    babel = require('gulp-babel'),
    imagemin = require('gulp-imagemin');

const reload = browserSync.reload;

//Html Task
gulp.task('html', function(){

    return gulp.src(['./src/templates/*.pug', '!./src/templates/includes/*'])
            .pipe(pug({ pretty: true}))
                .pipe(gulp.dest('./build'))
});

//css task
gulp.task('css', function(){

    var processors = [rucksack(), autoprefixer({browsers: 'last 4 versions'})];

    return gulp.src('./src/styles/*.scss')
            .pipe(sourcemaps.init())
            .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
            .pipe(postcss(processors))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('./build/css'))
            .pipe(browserSync.stream());
});

// js task
gulp.task('js', function(cb){

    pump([
        gulp.src('./src/js/*.js'),
        //sourcemaps.init(),
        babel({presets: ['@babel/env']}),
        //concat('main.bundle.js'),
        uglify(),
        //sourcemaps.write(),
        gulp.dest('./build/js')

    ], cb );
});

//optimize images
gulp.task('images', function(){

    return gulp.src('./src/images/**/*')
            .pipe(imagemin())
            .pipe(gulp.dest('./build/images'));

});

//Default task
gulp.task('default', ['html', 'css', 'js','images']);

gulp.task('pug-watch', ['html'] , function(done){
    browserSync.reload();
    done();
});

gulp.task('watch', ['default'], function(){

    gulp.watch('./src/templates/**/*', ['pug-watch']);

    //watch styles changes
    gulp.watch('./src/styles/**/*', ['css']);

    // gulp.watch('./build/css/**/*').on('change', browserSync.reload);

    //watch javascript changes
    gulp.watch('./src/js/*', ['js']).on('change', browserSync.reload);


    browserSync.init({
        startPath: "./build",
        server: {
            baseDir: "./",
            routes: {
                "bower_components": "./bower_components"
            }
        }
    });
});
