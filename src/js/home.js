'use strict';



// var shared = require('shared');

$(function(){

    /*
        Todo list
    */

    $(".js-to-do__chbx").on('click', function(){
        if($(this)[0].checked ){
            $(this).parent().find(".js-to-do__label").addClass('js-done');
        }else{
            $(this).parent().find(".js-to-do__label").removeClass('js-done');
        }
        
    });

    //Perfect scroll
    // const sideNavScroll = new PerfectScrollbar('.c-side-nav');
    // const messagesScroll = new PerfectScrollbar('.js-messages-ps');
    // const notifications = new PerfectScrollbar('.js-nots-ps');

    if ($('.c-chat-messages').length){
        const chatMessages = new PerfectScrollbar('.c-chat-messages');   
    }
    

    //data table
    $('#users').DataTable({
        "language": {
            "paginate": {
                "next": "<i class='material-icons'>keyboard_arrow_right</i>",
                "previous": "<i class='material-icons'>keyboard_arrow_left</i>"
            }
        }
    });


    
    // jqvmap

    if ( $("#vmapWorld").length ){

        $("#vmapWorld").vectorMap({ 
        
            backgroundColor: '#fff',
            borderColor: '#818181',
            borderOpacity: 0.25,
            borderWidth: 1,
            color: '#f4f3f0',
            enableZoom: true,
            hoverColor: '#c9dfaf',
            hoverOpacity: null,
            normalizeFunction: 'linear',
            scaleColors: ['#b6d6ff', '#005ace'],
            selectedColor: '#00a8ff',
            selectedRegions: null,
            showTooltip: true,
            selectedRegions: ['MA', 'US','SA','RU','AU']
        });

    }



    if ($("#chartist-1".length)){

        console.log('.good to go');

        var data = {
            // A labels array that can contain any sort of values
            labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
            // Our series array that contains series objects or in this case series data arrays
            series: [
              [50, 102, 140, 201, 240,200,550],
              [0, 202, 140, 101, 340,400,300]

            ]
          };
          
        var options = {
            fullWidth: true,
            chartPadding: {
                right: 40
            },
            low: 0,
            plugins: [
                Chartist.plugins.tooltip()
            ],
            lineSmooth: false
            
        }
          // Create a new line chart object where as first parameter we pass in a selector
          // that is resolving to our chart container element. The Second parameter
          // is the actual data object.
          new Chartist.Line('#chartist-1', data , options);
    }

    var chartjsOptions = {
        legend: {
            display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stacked: false
                },
                gridLines: {
                    // display: false,
                }
            }],
            xAxes: [{
                gridLines: {
                    display: false,
                }
            }]
        }
    };


    //Mounthly sales
    var salesCtx = document.getElementById('mounthlySales').getContext('2d');
    var salesData = {
        labels: ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept","Oct","Nov","Dec"],
        datasets: [
            {
                data: [302,345,245,456,444,678,567,678,686,788,976,956],
                backgroundColor: "rgba(55, 66, 250, 0)",
                borderColor: "#00a8ff",
                pointRadius: 0,
                borderWidth: 3
            },
            {
                data: [132,235,135,146,244,168,257,368,586,678,366,76],
                backgroundColor: "rgba(55, 66, 250, 0)",
                borderColor: "#dfe4ea",
                pointRadius: 0,
                borderWidth: 3
            }

        ]
    }
    var sales = new Chart(salesCtx, {
        type: 'line',
        data: salesData,
        options: chartjsOptions
    });



    var data = {
      series: [5, 3, 4]
    };

    var sum = function(a, b) { return a + b };

    new Chartist.Pie('.pie-chart', data, {
      labelInterpolationFnc: function(value) {
        return Math.round(value / data.series.reduce(sum) * 100) + '%';
      }
    });


});