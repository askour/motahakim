'use strict';

$(function(){

	/**********************************************************************

		1- Shared

	***********************************************************************/

    $("[href='#']").on('click' , function (e) {
        e.preventDefault();
    });

	/*
		Js Waves
	*/
	Waves.attach('.js-waves');
	Waves.attach('.js-waves--light', 'waves-light');
	Waves.attach('.js-waves--dark', 'waves-dark');
	Waves.init();

    //Perfect scroll
    let sideNavScroll = new PerfectScrollbar('.c-side-nav');
        if ($('.js-messages-ps').length) {
            const messagesScroll = new PerfectScrollbar('.js-messages-ps');
            const notifications = new PerfectScrollbar('.js-nots-ps');
        }



	/*
		navigation toggle
	*/
    $('.js-toggle-side-nav').on('click' , function(){
    	// console.log('ad')


        let width = $(window).width();
        

        if(width <= 1330){
            $('.o-main-page').toggleClass('is-open');            
        }else{


            $('.c-side-nav,.o-main-page').toggleClass('is-collapsed');

            if ($('.c-side-nav').hasClass('is-collapsed')) {

                $('.c-side-nav-logo__link img').attr('src', 'images/logo-mini.png');
                sideNavScroll.destroy();

            } else {
                $('.c-side-nav-logo__link img').attr('src', 'images/logo.png')
                sideNavScroll = new PerfectScrollbar('.c-side-nav');
            }
            $(this).find("i").toggleClass('ion-close');
        }
        
    });


    /*
    	Side nav dropdown
    */
    $(".c-side-nav__link").on('click', function(e){

        if (!$(this).next().hasClass('show')){
            $(this).find(".c-side-nav__drpdwn-icon").addClass('is-expanded');
        }else{
            $(this).find(".c-side-nav__drpdwn-icon").removeClass('is-expanded');            
        }

        $(".c-side-nav__link").each(function(link){
            if ($(this).next().hasClass('show')){
                $(this).find(".c-side-nav__drpdwn-icon").removeClass('is-expanded')
            }
        })
    });

    /*
    	tooltips

    */

    $('[data-toggle="tooltip"]').tooltip();

    /*
    	Todo list
    */

    if ($('.c-card').length ) {
        $('.minimizecard').on('click', function(e){

            // e.preventDefault();
            $(this).find('i').toggleClass('ion-ios-arrow-up');

            var parent =  $(this).parentsUntil('.c-card').parent().toggleClass('is-collapsed');
            console.log('sdff')
        });

        $('.closecard').on('click', function(e){

            e.preventDefault();

            console.log('dd');
           var card =  $(this).parentsUntil('.c-card').parent().parent();

           card.addClass('animated bounceOut');

           setTimeout(function(){
                card.css({
                    "display": "none"
                })

           }, 1000);

        });
    }



    console.log('Hi thereaa')
});
